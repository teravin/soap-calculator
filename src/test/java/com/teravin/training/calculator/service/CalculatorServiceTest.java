package com.teravin.training.calculator.service;

import com.teravin.training.calculator.config.CalculatorConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@Import(CalculatorConfig.class)
public class CalculatorServiceTest {

    @Autowired
    private CalculatorService calculatorService;

    @Test
    public void testAdd() {

        int result = calculatorService.add(5, 10);
        assertEquals(15, result);
    }

    @Test
    public void testSubtract() {

        int result = calculatorService.subtract(20, 8);
        assertEquals(12, result);
    }

    @Test
    public void testMultiply() {

        int result = calculatorService.multiply(3, 9);
        assertEquals(27, result);
    }

    @Test
    public void testDivide() {

        int result = calculatorService.divide(10, 5);
        assertEquals(2, result);
    }
}