package com.teravin.training.calculator.service.impl;

import com.teravin.training.calculator.model.*;
import com.teravin.training.calculator.service.CalculatorService;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

public class CalculatorServiceImpl extends WebServiceGatewaySupport implements CalculatorService {

    @Override
    public int add(int first, int second) {

        Add request = new Add();
        request.setIntA(first);
        request.setIntB(second);

        AddResponse response = (AddResponse) getWebServiceTemplate().marshalSendAndReceive(request, new SoapActionCallback("http://tempuri.org/Add"));
        return response.getAddResult();
    }

    @Override
    public int subtract(int first, int second) {

        Subtract request = new Subtract();
        request.setIntA(first);
        request.setIntB(second);

        SubtractResponse response = (SubtractResponse) getWebServiceTemplate().marshalSendAndReceive(request, new SoapActionCallback("http://tempuri.org/Subtract"));
        return response.getSubtractResult();
    }

    @Override
    public int multiply(int first, int second) {

        Multiply request = new Multiply();
        request.setIntA(first);
        request.setIntB(second);

        MultiplyResponse response = (MultiplyResponse) getWebServiceTemplate().marshalSendAndReceive(request, new SoapActionCallback("http://tempuri.org/Multiply"));
        return response.getMultiplyResult();
    }

    @Override
    public int divide(int first, int second) {

        Divide request = new Divide();
        request.setIntA(first);
        request.setIntB(second);

        DivideResponse response = (DivideResponse) getWebServiceTemplate().marshalSendAndReceive(request, new SoapActionCallback("http://tempuri.org/Divide"));
        return response.getDivideResult();
    }
}