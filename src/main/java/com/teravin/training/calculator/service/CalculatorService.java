package com.teravin.training.calculator.service;

public interface CalculatorService {

    int add(int first, int second);

    int subtract(int first, int second);

    int multiply(int first, int second);

    int divide(int first, int second);
}
