package com.teravin.training.calculator.config;

import com.teravin.training.calculator.service.CalculatorService;
import com.teravin.training.calculator.service.impl.CalculatorServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

@Configuration
public class CalculatorConfig {

    @Bean
    public HttpComponentsMessageSender calculatorHttpComponentsMessageSender() {
        return new HttpComponentsMessageSender();
    }

    @Bean
    public Jaxb2Marshaller calculatorMarshaller() {

        Jaxb2Marshaller marshallerWirecard = new Jaxb2Marshaller();
        marshallerWirecard.setPackagesToScan("com.teravin.training.calculator.model");
        return marshallerWirecard;
    }

    @Bean(name = "calculatorWebServiceTemplate")
    public WebServiceTemplate calculatorWebServiceTemplate() {

        WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
        webServiceTemplate.setDefaultUri("http://www.dneonline.com/calculator.asmx");
        webServiceTemplate.setUnmarshaller(calculatorMarshaller());
        webServiceTemplate.setMarshaller(calculatorMarshaller());
        webServiceTemplate.setMessageSender(calculatorHttpComponentsMessageSender());
        return webServiceTemplate;
    }

    @Bean
    public CalculatorService calculatorService() {

        CalculatorServiceImpl calculatorService = new CalculatorServiceImpl();
        calculatorService.setWebServiceTemplate(calculatorWebServiceTemplate());
        return calculatorService;
    }
}